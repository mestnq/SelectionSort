﻿using System;

namespace Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] data =
            {
                571, 50, 712, 134, 68, 235, 4, 820, 304, 764, 780, 929, 946, 448, 395, 609, 513, 635, 944, 348, 749,
                397, 283, 311, 692, 2, 704, 715, 841, 923, 258, 265, 2, 714, 376, 193, 240, 441, 657, 585, 197, 563,
                303, 963, 199, 114, 478, 848, 761, 678, 863, 255, 401, 471, 656, 966, 547, 596, 331, 198, 634, 188, 154,
                166, 618, 552, 2, 721, 196, 205, 992, 240, 61, 645, 444, 521, 551, 873, 481, 365, 647, 52, 352, 66, 117,
                867, 95, 256, 530, 977, 70, 94, 740, 56, 882, 701, 879, 46, 543, 787
            };
            int indexLastSortedItem = 0;
            while (indexLastSortedItem != data.Length - 1)
            {
                int indexMinItem = indexLastSortedItem;
                for (int i = indexLastSortedItem; i < data.Length; i++)
                {
                    if (data[indexMinItem] > data[i])
                    {
                        indexMinItem = i;
                    }
                }

                (data[indexMinItem], data[indexLastSortedItem]) = (data[indexLastSortedItem], data[indexMinItem]);
                indexLastSortedItem++;
            }

            foreach (var el in data)
            {
                Console.WriteLine($"{el}");
            }
        }
    }
}